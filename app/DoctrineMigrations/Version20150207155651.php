<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150207155651 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

        $this->addSql("UPDATE improve_core_tag SET slug='sport' WHERE name='sport'");
        $this->addSql("UPDATE improve_core_tag SET slug='seks' WHERE name='seks'");
        $this->addSql("UPDATE improve_core_tag SET slug='samochody' WHERE name='samochody'");
        $this->addSql("UPDATE improve_core_tag SET slug='kobiety' WHERE name='kobiety'");
        $this->addSql("UPDATE improve_core_tag SET slug='plotki' WHERE name='plotki'");
        $this->addSql("UPDATE improve_core_tag SET slug='zycie-gwiazd' WHERE name='zycie gwiazd'");
        $this->addSql("UPDATE improve_core_tag SET slug='uroda' WHERE name='uroda'");
        $this->addSql("UPDATE improve_core_tag SET slug='kobieta' WHERE name='kobieta'");
        $this->addSql("UPDATE improve_core_tag SET slug='facet' WHERE name='facet'");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
        
        $this->addSql('CREATE SCHEMA public');
    }
}
