<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20141006194507 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
        
        $this->addSql('CREATE TABLE maw_news_tags (news_id INT NOT NULL, tag_id INT NOT NULL, PRIMARY KEY(news_id, tag_id))');
        $this->addSql('CREATE INDEX IDX_36323073B5A459A0 ON maw_news_tags (news_id)');
        $this->addSql('CREATE INDEX IDX_36323073BAD26311 ON maw_news_tags (tag_id)');
        $this->addSql('ALTER TABLE maw_news_tags ADD CONSTRAINT FK_36323073B5A459A0 FOREIGN KEY (news_id) REFERENCES maw_news_news (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE maw_news_tags ADD CONSTRAINT FK_36323073BAD26311 FOREIGN KEY (tag_id) REFERENCES maw_news_news (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE maw_news_news ADD category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE maw_news_news ADD CONSTRAINT FK_445D3D0512469DE2 FOREIGN KEY (category_id) REFERENCES improve_core_tag (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_445D3D0512469DE2 ON maw_news_news (category_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
    }
}
