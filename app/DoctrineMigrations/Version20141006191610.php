<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20141006191610 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("INSERT INTO improve_core_tag (id, name, type, created, modified) VALUES (1, 'sport', 'tag', '2014-10-01 00:00:00', '2014-10-01 00:00:00')");
        $this->addSql("INSERT INTO improve_core_tag (id, name, type, created, modified) VALUES (2, 'seks', 'tag', '2014-10-01 00:00:00', '2014-10-01 00:00:00')");
        $this->addSql("INSERT INTO improve_core_tag (id, name, type, created, modified) VALUES (3, 'samochody', 'tag', '2014-10-01 00:00:00', '2014-10-01 00:00:00')");
        $this->addSql("INSERT INTO improve_core_tag (id, name, type, created, modified) VALUES (4, 'kobiety', 'tag', '2014-10-01 00:00:00', '2014-10-01 00:00:00')");
        $this->addSql("INSERT INTO improve_core_tag (id, name, type, created, modified) VALUES (5, 'plotki', 'tag', '2014-10-01 00:00:00', '2014-10-01 00:00:00')");
        $this->addSql("INSERT INTO improve_core_tag (id, name, type, created, modified) VALUES (6, 'życie gwiazd', 'tag', '2014-10-01 00:00:00', '2014-10-01 00:00:00')");
        $this->addSql("INSERT INTO improve_core_tag (id, name, type, created, modified) VALUES (7, 'uroda', 'tag', '2014-10-01 00:00:00', '2014-10-01 00:00:00')");
        $this->addSql("INSERT INTO improve_core_tag (id, name, type, created, modified) VALUES (8, 'kobieta', 'category', '2014-10-01 00:00:00', '2014-10-01 00:00:00')");
        $this->addSql("INSERT INTO improve_core_tag (id, name, type, created, modified) VALUES (9, 'facet', 'category', '2014-10-01 00:00:00', '2014-10-01 00:00:00')");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

    }
}
