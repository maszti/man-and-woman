<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150124201122 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
        
        $this->addSql('CREATE SEQUENCE maw_message_post_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE maw_message_board_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE maw_message_post (id INT NOT NULL, board_id INT DEFAULT NULL, author VARCHAR(255) DEFAULT NULL, content VARCHAR(500) NOT NULL, count_like INT NOT NULL, count_unlike INT NOT NULL, created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_EE7D07AE7EC5785 ON maw_message_post (board_id)');
        $this->addSql('CREATE TABLE maw_message_board (id INT NOT NULL, count_post INT NOT NULL, created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE maw_message_post ADD CONSTRAINT FK_EE7D07AE7EC5785 FOREIGN KEY (board_id) REFERENCES maw_message_board (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE maw_news_news ADD board_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE maw_news_news ADD CONSTRAINT FK_445D3D05E7EC5785 FOREIGN KEY (board_id) REFERENCES maw_message_board (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_445D3D05E7EC5785 ON maw_news_news (board_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
    }
}
