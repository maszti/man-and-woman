<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20141003183225 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE maw_news_news_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE maw_news_news (id INT NOT NULL, title VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, excerpt TEXT NOT NULL, content TEXT NOT NULL, published BOOLEAN NOT NULL, publishDate TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, modified TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, excerptMedia_id INT DEFAULT NULL, contentMedia_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_445D3D05989D9B62 ON maw_news_news (slug)');
        $this->addSql('CREATE INDEX IDX_445D3D05F717644D ON maw_news_news (excerptMedia_id)');
        $this->addSql('CREATE INDEX IDX_445D3D05B8A987ED ON maw_news_news (contentMedia_id)');
        $this->addSql('ALTER TABLE maw_news_news ADD CONSTRAINT FK_445D3D05F717644D FOREIGN KEY (excerptMedia_id) REFERENCES media__media (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE maw_news_news ADD CONSTRAINT FK_445D3D05B8A987ED FOREIGN KEY (contentMedia_id) REFERENCES media__media (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
    }
}
