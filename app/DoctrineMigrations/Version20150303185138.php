<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150303185138 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

        $this->addSql("INSERT INTO classification__context(id, name, enabled, created_at, updated_at) VALUES ('gallery_upload', 'gallery_upload', TRUE, '2015-03-03 18:29:13', '2015-03-03 18:29:13')");
        $this->addSql("INSERT INTO classification__category(id, context, name, enabled, slug, description, created_at, updated_at) VALUES (3, 'gallery_upload', 'gallery_upload', TRUE, 'gallery-upload', 'gallery_upload', '2015-03-03 18:29:13', '2015-03-03 18:29:13')");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
    }
}
