<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20141211101933 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
        
        $this->addSql('DROP SEQUENCE fos_user_group_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE fos_user_user_id_seq CASCADE');
        $this->addSql('DROP TABLE fos_user_user');
        $this->addSql('DROP TABLE fos_user_group');
        $this->addSql('ALTER TABLE improve_user ADD agreeRegulation BOOLEAN NOT NULL DEFAULT true');
        $this->addSql("ALTER TABLE improve_user ALTER COLUMN agreeRegulation DROP DEFAULT");

        $this->addSql('ALTER TABLE improve_user ADD newsletter BOOLEAN NOT NULL DEFAULT true');
        $this->addSql("ALTER TABLE improve_user ALTER COLUMN newsletter DROP DEFAULT");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
    }
}
