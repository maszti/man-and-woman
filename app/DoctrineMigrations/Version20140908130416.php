<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140908130416 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("CREATE SEQUENCE newsletter_sender_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
        $this->addSql("CREATE TABLE newsletter_sender (id INT NOT NULL, newsletter_id INT DEFAULT NULL, user_id INT DEFAULT NULL, send BOOLEAN NOT NULL, PRIMARY KEY(id))");
        $this->addSql("CREATE INDEX IDX_E55A118622DB1917 ON newsletter_sender (newsletter_id)");
        $this->addSql("CREATE INDEX IDX_E55A1186A76ED395 ON newsletter_sender (user_id)");
        $this->addSql("ALTER TABLE newsletter_sender ADD CONSTRAINT FK_E55A118622DB1917 FOREIGN KEY (newsletter_id) REFERENCES newsletter_newsletter (id) NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("ALTER TABLE newsletter_sender ADD CONSTRAINT FK_E55A1186A76ED395 FOREIGN KEY (user_id) REFERENCES improve_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
    }
}
