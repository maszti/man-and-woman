<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140907115700 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("INSERT INTO improve_user_group(name, roles) VALUES ('company' , 'a:1:{i:0;s:9:\"ROLE_USER\";}')");
        $this->addSql("INSERT INTO improve_user_group(name, roles) VALUES ('private_user' , 'a:1:{i:0;s:9:\"ROLE_USER\";}')");
        $this->addSql("INSERT INTO improve_user_group(name, roles) VALUES ('gmdt' , 'a:1:{i:0;s:9:\"ROLE_USER\";}')");
        $this->addSql("INSERT INTO improve_user_group(name, roles) VALUES ('40minut' , 'a:1:{i:0;s:9:\"ROLE_USER\";}')");
        $this->addSql("INSERT INTO improve_user_group(name, roles) VALUES ('pentagos' , 'a:1:{i:0;s:9:\"ROLE_USER\";}')");
        $this->addSql("INSERT INTO improve_user_group(name, roles) VALUES ('nefretete' , 'a:1:{i:0;s:9:\"ROLE_USER\";}')");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
    }
}
