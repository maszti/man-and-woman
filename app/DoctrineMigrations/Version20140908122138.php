<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140908122138 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("CREATE TABLE newsletter_accepted_group (newsletter_id INT NOT NULL, group_id INT NOT NULL, PRIMARY KEY(newsletter_id, group_id))");
        $this->addSql("CREATE INDEX IDX_7B3A2B722DB1917 ON newsletter_accepted_group (newsletter_id)");
        $this->addSql("CREATE INDEX IDX_7B3A2B7FE54D947 ON newsletter_accepted_group (group_id)");
        $this->addSql("CREATE TABLE newsletter_declined_group (newsletter_id INT NOT NULL, group_id INT NOT NULL, PRIMARY KEY(newsletter_id, group_id))");
        $this->addSql("CREATE INDEX IDX_BAED4F4F22DB1917 ON newsletter_declined_group (newsletter_id)");
        $this->addSql("CREATE INDEX IDX_BAED4F4FFE54D947 ON newsletter_declined_group (group_id)");
        $this->addSql("ALTER TABLE newsletter_accepted_group ADD CONSTRAINT FK_7B3A2B722DB1917 FOREIGN KEY (newsletter_id) REFERENCES newsletter_newsletter (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("ALTER TABLE newsletter_accepted_group ADD CONSTRAINT FK_7B3A2B7FE54D947 FOREIGN KEY (group_id) REFERENCES improve_user_group (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("ALTER TABLE newsletter_declined_group ADD CONSTRAINT FK_BAED4F4F22DB1917 FOREIGN KEY (newsletter_id) REFERENCES newsletter_newsletter (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("ALTER TABLE newsletter_declined_group ADD CONSTRAINT FK_BAED4F4FFE54D947 FOREIGN KEY (group_id) REFERENCES improve_user_group (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("DROP TABLE newsletter_group");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
    }
}
