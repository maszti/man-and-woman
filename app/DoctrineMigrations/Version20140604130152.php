<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140604130152 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");

        $this->addSql("ALTER TABLE newsletter_user_group DROP CONSTRAINT fk_6217863722db1917");
        $this->addSql("DROP SEQUENCE newsletter_id_seq CASCADE");
        $this->addSql("CREATE SEQUENCE newsletter_newsletter_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
        $this->addSql("CREATE TABLE newsletter_newsletter (id INT NOT NULL, name VARCHAR(255) NOT NULL, short_description VARCHAR(255) NOT NULL, content TEXT NOT NULL, created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, modified TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, count_send INT NOT NULL, PRIMARY KEY(id))");
        $this->addSql("CREATE TABLE newsletter_group (newsletter_id INT NOT NULL, group_id INT NOT NULL, PRIMARY KEY(newsletter_id, group_id))");
        $this->addSql("CREATE INDEX IDX_E7AD083122DB1917 ON newsletter_group (newsletter_id)");
        $this->addSql("CREATE INDEX IDX_E7AD0831FE54D947 ON newsletter_group (group_id)");
        $this->addSql("ALTER TABLE newsletter_group ADD CONSTRAINT FK_E7AD083122DB1917 FOREIGN KEY (newsletter_id) REFERENCES newsletter_newsletter (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("ALTER TABLE newsletter_group ADD CONSTRAINT FK_E7AD0831FE54D947 FOREIGN KEY (group_id) REFERENCES fos_user_group (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("DROP TABLE newsletter_user_group");
        $this->addSql("DROP TABLE newsletter");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
    }
}
