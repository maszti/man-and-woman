<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20141002172334 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
        
        $this->addSql('CREATE TABLE article_article (id SERIAL NOT NULL, author_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, excerpt TEXT NOT NULL, excerptMarkdown TEXT DEFAULT NULL, content TEXT NOT NULL, markdown TEXT DEFAULT NULL, published BOOLEAN NOT NULL, unofficial BOOLEAN NOT NULL, publishDate TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, modified TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, views INT NOT NULL, excerptMedia_id INT DEFAULT NULL, contentMedia_id INT DEFAULT NULL, dtype VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_EFE84AD1989D9B62 ON article_article (slug)');
        $this->addSql('CREATE INDEX IDX_EFE84AD1F675F31B ON article_article (author_id)');
        $this->addSql('CREATE INDEX IDX_EFE84AD1F717644D ON article_article (excerptMedia_id)');
        $this->addSql('CREATE INDEX IDX_EFE84AD1B8A987ED ON article_article (contentMedia_id)');
        $this->addSql('CREATE INDEX IDX_EFE84AD170AAEA5 ON article_article (dtype)');
        $this->addSql('ALTER TABLE article_article ADD CONSTRAINT FK_EFE84AD1F675F31B FOREIGN KEY (author_id) REFERENCES improve_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE article_article ADD CONSTRAINT FK_EFE84AD1F717644D FOREIGN KEY (excerptMedia_id) REFERENCES media__media (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE article_article ADD CONSTRAINT FK_EFE84AD1B8A987ED FOREIGN KEY (contentMedia_id) REFERENCES media__media (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

    }
}
