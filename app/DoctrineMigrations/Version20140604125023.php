<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140604125023 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");

        $this->addSql("CREATE TABLE newsletter_user_group (newsletter_id INT NOT NULL, group_id INT NOT NULL, PRIMARY KEY(newsletter_id, group_id))");
        $this->addSql("CREATE INDEX IDX_6217863722DB1917 ON newsletter_user_group (newsletter_id)");
        $this->addSql("CREATE INDEX IDX_62178637FE54D947 ON newsletter_user_group (group_id)");
        $this->addSql("ALTER TABLE newsletter_user_group ADD CONSTRAINT FK_6217863722DB1917 FOREIGN KEY (newsletter_id) REFERENCES Newsletter (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("ALTER TABLE newsletter_user_group ADD CONSTRAINT FK_62178637FE54D947 FOREIGN KEY (group_id) REFERENCES fos_user_group (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
    }
}
