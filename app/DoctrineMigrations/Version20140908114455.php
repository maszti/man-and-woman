<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140908114455 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("INSERT INTO improve_user_group(name, roles) VALUES ('testowy' , 'a:1:{i:0;s:9:\"ROLE_USER\";}')");

    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
    }
}
