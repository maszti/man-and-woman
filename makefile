clean:
	php app/console assets:install
	php app/console cache:clear --env=prod
	php app/console cache:clear

assetic:
	php app/console assets:install web
	php app/console assetic:dump -e=prod

install: do_install clean

do_install:
	php composer.phar install -o
	-php app/console doctrine:database:create
	php app/console doctrine:migrations:migrate --no-interaction
	php app/console sonata:admin:setup-acl

update: do_update assetic clean access


recreate_db:
	-php app/console doctrine:database:drop --force
	php app/console doctrine:database:create

do_update:
	git pull
	php composer.phar install -o
	php app/console doctrine:migrations:migrate --no-interaction

upgrade: update
	php composer.phar update

cleans: clean
    php app/console assets:install --symlink

push:
	git stash clear
	git stash
	git pull --rebase
	git push
	-git stash apply

rsync:
	export RSYNC_PASSWORD=ZGV2ZWxvcGVyc2tpZQ && rsync -rv rsync://developer@gamedot.pl/media web/uploads

access:
	chmod -R 775 app/cache app/logs

prod: update assetic access
