<?php

namespace Application\Sonata\MediaBundle\Admin\ORM;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\MediaBundle\Admin\ORM\MediaAdmin as Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\MediaBundle\Form\DataTransformer\ProviderDataTransformer;

class MediaAdmin extends Admin
{
    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $media = $this->getSubject();

        if (!$media) {
            $media = $this->getNewInstance();
        }

        if (!$media || !$media->getProviderName()) {
            return;
        }

        $formMapper->add('providerName', 'hidden');

        $formMapper->getFormBuilder()->addModelTransformer(new ProviderDataTransformer($this->pool, $this->getClass()), true);

        $provider = $this->pool->getProvider($media->getProviderName());

        if ($media->getId()) {
            $provider->buildEditForm($formMapper);
        } else {
            $provider->buildCreateForm($formMapper);
        }

        $context = $media->getContext();

        $formMapper->add('category', 'sonata_type_model_list',
            ['label' => 'Kategoria obrazka'], [
            'link_parameters' => [
                'context'      => $context,
                'mode'         => 'tree'
            ]
        ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name', null, ['label' => 'Nazwa'])
            ->add('description', null, ['label' => 'Opis']);
    }

    /**
     * @param  \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $options = array(
            'choices' => array()
        );

        foreach ($this->pool->getContexts() as $name => $context) {
            $options['choices'][$name] = $name;
        }

        $datagridMapper
            ->add('name', null, ["label" => "Nazwa"])
            ->add('providerReference', null, ["label" => "Referencje dostawcy"])
            ->add('enabled', null, ["label" => "Włączony"])
            ->add('context', null, [
                'label' => 'Kontekst',
                'show_filter' => $this->getPersistentParameter('hide_context') !== true
            ], 'choice', $options)
            ->add('category', null, [
                'label' => 'Kategorie',
                'show_filter' => false,
            ])
            ->add('contentType', null, ['label' => 'Typ kontekstu']);
    }
}
