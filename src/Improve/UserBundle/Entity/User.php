<?php
namespace Improve\UserBundle\Entity;

use Sonata\UserBundle\Entity\BaseUser as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Improve\UserBundle\Entity\UserRepository")
 * @ORM\Table(name="improve_user")
 * @ORM\HasLifecycleCallbacks
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $dateBirthday;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $address;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $agreeRegulation = true;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $newsletter = false;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return boolean
     */
    public function isAgreeRegulation()
    {
        return $this->agreeRegulation;
    }

    /**
     * @param boolean $agreeRegulation
     */
    public function setAgreeRegulation($agreeRegulation)
    {
        $this->agreeRegulation = $agreeRegulation;
    }

    /**
     * @return boolean
     */
    public function isNewsletter()
    {
        return $this->newsletter;
    }

    /**
     * @param boolean $newsletter
     */
    public function setNewsletter($newsletter)
    {
        $this->newsletter = $newsletter;
    }

    /**
     * Set dateBirthday
     *
     * @param string $dateBirthday
     * @return User
     */
    public function setDateBirthday($dateBirthday)
    {
        $this->dateBirthday = $dateBirthday;

        return $this;
    }

    /**
     * Get dateBirthday
     *
     * @return string
     */
    public function getDateBirthday()
    {
        return $this->dateBirthday;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }
}
