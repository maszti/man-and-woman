<?php

namespace Improve\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Improve\UserBundle\Entity\User;

class ProfileController extends Controller
{
    /**
     * @Route("/list", options={"expose"=true})
     * @Template()
     */
    public function indexAction(){
        return compact([]);
    }
}
