<?php

namespace Improve\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

class ImportController extends Controller
{
    /**
     * @Route("/parse-user-gm", options={"expose"=true})
     */
    public function importDataFromGdAction()
    {
        $em = $this->getDoctrine()->getManager();
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject('users/users_12_02_15.ods');
        $this->get('improve.import.client.manager')->addNewUserGd($phpExcelObject);

        $em->flush();

        return new Response("OK");
    }

    /**
     * @Route("/parse-user-pentagos-hostess", options={"expose"=true})
     */
    public function importDataFromPentagosHostessAction()
    {
        $em = $this->getDoctrine()->getManager();
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject('users/usersPentagos.ods');
        $this->get('improve.import.client.manager')->addNewUsersPentagosHostess($phpExcelObject);

        $em->flush();

        return new Response("OK");
    }

    /**
     * @Route("/parse-user-pentagos-company", options={"expose"=true})
     */
    public function importDataFromPentagosCompanyAction()
    {
        $em = $this->getDoctrine()->getManager();
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject('users/usersPentagosFirmy.ods');
        $this->get('improve.import.client.manager')->addNewUsersPentagosCompany($phpExcelObject);

        $em->flush();

        return new Response("OK");
    }

    /**
     * @Route("/parse-user-40minut", options={"expose"=true})
     */
    public function importDataFromPentagos40minutAction()
    {
        $em = $this->getDoctrine()->getManager();
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject('users/users40minut.ods');
        $this->get('improve.import.client.manager')->addNewUsers40minut($phpExcelObject);

        $em->flush();

        return new Response("OK");
    }

    /**
     * @Route("/parse-user-nefretete", options={"expose"=true})
     */
    public function importDataFromPentagosNefreteteAction()
    {
        $em = $this->getDoctrine()->getManager();
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject('users/usersNefretete.ods');
        $this->get('improve.import.client.manager')->addNewUsersNefretete($phpExcelObject);

        $em->flush();

        return new Response("OK");
    }

    /**
     * @Route("/parse-user-test", options={"expose"=true})
     */
    public function importDataFromPentagosTestAction()
    {
        $em = $this->getDoctrine()->getManager();
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject('users/usersTest.ods');
        $this->get('improve.import.client.manager')->addNewUsersTest($phpExcelObject);

        $em->flush();

        return new Response("OK");
    }

    /**
     * @Route("/send-email", options={"expose"=true})
     * @Template()
     */
    public function sendEmailAction()
    {
        $mailer = $this->get('swiftmailer.mailer.newsletter');
        for($i=0; $i < 3; $i++) {
            $message = \Swift_Message::newInstance()
                    ->setSubject('BIURO E_IMPROVE')
                    ->setFrom('dmasztalerz@e-improve.pl')
                    ->setTo(['darek.masztalerz@gmail.com'])
                    ->setBody($this->renderView('ImproveUserBundle:Email:test.html.twig',
                              compact('content')), 'text/html');

            $mail = $mailer->send($message);
        }

        return new Response("OK");

    }
}
