<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Improve\UserBundle\Controller;

use FOS\UserBundle\Controller\RegistrationController as FOSRegistrationController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller managing the registration
 *
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 * @author Christophe Coevoet <stof@notk.org>
 */
class RegistrationController extends FOSRegistrationController
{
    public function registerAction()
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        if (is_object($user) && $user instanceof UserInterface) {
            $url = $this->container->get('router')->generate('maw_portal_main_index');
            return new RedirectResponse($url);
        }

        $form = $this->container->get('fos_user.registration.form');
        $formHandler = $this->container->get('fos_user.registration.form.handler');
        $confirmationEnabled = $this->container->getParameter('fos_user.registration.confirmation.enabled');
        $process = $formHandler->process($confirmationEnabled);
        if ($process) {
            $user = $form->getData();
            $this->setFlash('success', 'Konto użytkownika zostało utworzone.');

            $authUser = false;
            if ($confirmationEnabled) {
                $this->setFlash('success', "Na adres {$user->getEmail()} wysłano wiadomość e-mail. Zawiera ona link w którego należy kliknąć aby aktywować konto.");
                $route = 'maw_news_news_index';
            } else {
                $authUser = true;
                $route = 'fos_user_registration_confirmed';
            }

            $url = $this->container->get('router')->generate($route);
            $response = new RedirectResponse($url);

            if ($authUser) {
                $this->authenticateUser($user, $response);
            }

            return $response;
        }

        return $this->container->get('templating')->renderResponse('ImproveUserBundle:Registration:register.html.twig', [
            'form' => $form->createView(),
            'no_button' => true
        ]);
    }

    public function checkAction() {
        $request = $this->container->get('request');
        $nickname = $request->query->get('nickname', null);
        $username = $request->query->get('username', null);
        $email = $request->query->get('email', null);
        $userManager = $this->container->get('fos_user.user_manager');
        if($nickname){
            $user = $userManager->findUserBy(['nickname' => $nickname]);
        } elseif($username) {
            $user = $userManager->findUserBy(['username' => $username]);
        } elseif($email) {
            $user = $userManager->findUserBy(['email' => $email]);
        } else {
            $user = null;
        }
        return new Response($user?"1":"0");
    }

    public function confirmAction($token){
        $user = $this->container->get('fos_user.user_manager')->findUserByConfirmationToken($token);
        if (null === $user) {
            $session = $this->container->get('session');
            $session->getFlashBag()->add('error', 'Nieprawidłowy token, czyżby twoje konto było już aktywne?');
            $url = $this->container->get('router')->generate('maw_portal_main_index');
            return new RedirectResponse($url);
        }
        return $this->confirm($token);
    }

    private function confirm($token) {
        $user = $this->container->get('fos_user.user_manager')->findUserByConfirmationToken($token);

        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with confirmation token "%s" does not exist', $token));
        }

        $user->setConfirmationToken(null);
        $user->setEnabled(true);
        $user->setLastLogin(new \DateTime());

        $this->container->get('fos_user.user_manager')->updateUser($user);
        $session = $this->container->get('session');
        $session->getFlashBag()->add('success', 'Gratulacje. Twoje konto zostało poprawnie potwierdzone. Zostałeś automatycznie zalogowany do systemu.');
        $response = new RedirectResponse($this->container->get('router')->generate('maw_portal_main_index'));
        $this->authenticateUser($user, $response);

        return $response;
    }

    public function fbRegistrationFinaliseAction(Request $request)
    {
        $form = $this->container->get('fos_user.registration.form');
        $session = $this->container->get('session');
        $userManager = $this->container->get('fos_user.user_manager');

        if (!$session->get('facebook_login')) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $form->remove('captcha');
        $userData = $session->get('facebook_userdata');
        $user = $userManager->createUser();

        $user->setEmail($userData['email']);
        $user->setUsername($userData['username']);
        $user->setFBData($userData['fbData']);
        $user->setEnabled(true);

        $form->setData($user);

        $similarUser = $userManager->findUserBy(['email' => $userData['email']]);
        if ('POST' === $request->getMethod() && $form->submit($request) && $form->isValid()) {
            $userManager->updateUser($user);
            $session->getFlashBag()->add('success', 'Gratulacje, Twoje konto zostało aktywowane.');
            $url = $this->container->get('router')->generate('improve_portal_main_index');
            $response = new RedirectResponse($url);
            $this->authenticateUser($user, $response);
            $session->remove('facebook_login');
            return $response;
        }

        return $this->container->get('templating')->renderResponse('FOSUserBundle:Registration:register.html.' . $this->getEngine(), [
            'similarUser' => $similarUser,
            'form' => $form->createView(),
        ]);
    }

    public function fbRegistrationCancelAction(Request $request)
    {
        $url = $this->container->get('router')->generate('improve_shop_products_mainpage');
        $session = $this->container->get('session');
        $session->remove('facebook_login');
        $response = new RedirectResponse($url);
        return $response;
    }

    public function quickRegistrationAction(Request $request)
    {
        $this->requireAdminAccess();
        $emailForm = $this->getQuickRegistrationForm();

        if("POST" == $request->getMethod()){
            $emailForm->submit($request);
            return $this->processQuickRegistration($emailForm->get('email')->getData());
        }
        return $this->renderQuickRegistrationTemplate($emailForm);
    }

    private function renderQuickRegistrationTemplate($emailForm){
        return $this->container->get('templating')
            ->renderResponse('ImproveUserBundle:Registration:quickRegister.html.twig', ['form' => $emailForm->createView()]);
    }

    private function getQuickRegistrationForm(){
        return $this->container->get('form.factory')->create(new QuickRegistrationFormType());
    }

    private function requireAdminAccess(){
        if (false == $this->currentUserHasAdminAccess()) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }
    }

    private function currentUserHasAdminAccess(){
        return $this->container->get('security.context')->isGranted('ROLE_ADMIN');
    }

    private function processQuickRegistration($email){
        $userManager = $this->container->get('fos_user.user_manager');
        list($newUser, $password) = $userManager->quickRegisterUser($email);
        $newUser && $this->handleQuickRegisteredUserNotifications($newUser, $password);
        $newUser || $this->container->get('session')->getFlashBag()->add('warning', 'Problem z rejestracją konta');

        return new RedirectResponse($this->container->get('router')->generate('improve_user_registration_quick'));
    }

    private function handleQuickRegisteredUserNotifications($user, $password){
        $this->container->get('session')->getFlashBag()->add('success', 'Nowe konto zostało zarejestrowane');
        $this->sendQuickRegistrationEmail($user, $password);
    }

    private function sendQuickRegistrationEmail($user, $password){
        $this->container->get('fos_user.mailer')->sendQuickRegistrationEmailMessage($user, $password);
    }

}
