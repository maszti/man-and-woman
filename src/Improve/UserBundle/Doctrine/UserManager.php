<?php

namespace Improve\UserBundle\Doctrine;

use FOS\UserBundle\Doctrine\UserManager as BaseUserManager;
use Sonata\UserBundle\Model\UserInterface;

class UserManager extends BaseUserManager
{

    /**
     * Register new user instantly
     *
     * @param string $email
     * @return array [User $user, string $password]
     */
    public function quickRegisterUser($email)
    {
        $user = $this->createUser();

        if ($this->findUserBy(compact('email'))) {
            return [null, null];
        }

        $user->setEmail($email);

        $password = $this->setUserData($user);

        $this->updateUser($user);

        return [$user, $password];
    }

    /**
     * @param UserInterface $user
     * @return array
     */
    public function quickRegisterUserSonata(UserInterface $user)
    {
        if ($this->findUserBy(['email' => $user->getEmail()])) {
            return [null, null];
        }

        $password = $this->setUserData($user);

        $this->updateUser($user, false);

        return [$user, $password];
    }

    /**
     * @param UserInterface $user
     * @return string
     */
    private function setUserData(UserInterface $user)
    {
        $username = strstr($user->getEmail(), '@', true);
        $username = preg_replace('/[^\w\.\-_]/', '', $username);

        while ($this->findUserBy(['username' => $username])) {
            $username .= rand(0, 9);
        }

        $password = substr(md5(uniqid()), 0, 12);
        $user->setPlainPassword($password);
        $user->setUsername($username);
        $user->setEnabled(true);

        return $password;
    }
}
