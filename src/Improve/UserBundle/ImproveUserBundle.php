<?php

namespace Improve\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ImproveUserBundle extends Bundle
{
    public function getParent()
    {
        return 'SonataUserBundle';
    }
}
