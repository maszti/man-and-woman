<?php

namespace Improve\UserBundle\Form\Handler;

use FOS\UserBundle\Form\Handler as Handler;

class RegistrationFormApiHandler extends Handler
{
    public function disableCaptcha() {
        $this->form->remove('captcha');
    }
}
