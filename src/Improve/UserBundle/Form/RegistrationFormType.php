<?php

namespace Improve\UserBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RegistrationFormType extends BaseType
{
    public function __construct($class, $router) {
        parent::__construct($class);
        $this->router = $router;
    }

    /**
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('captcha', "genemu_captcha", array('label' => 'Wpisz kod z obrazka:', "mapped" => false));
        $builder->add('agreeRegulation', "checkbox", array('attr' => array('checked'   => 'checked')));
        $builder->add('newsletter', "checkbox", array('attr' => array('checked'   => 'checked')));
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'      => 'Improve\UserBundle\Entity\User',
            'csrf_protection' => false
        ));
    }

    public function getName()
    {
        return 'maw_user_registration';
    }
}
