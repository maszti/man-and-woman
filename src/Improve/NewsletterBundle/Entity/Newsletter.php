<?php

namespace Improve\NewsletterBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Newsletter
 *
 * @ORM\Table("newsletter_newsletter")
 * @ORM\Entity
 */
class Newsletter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="template", type="string", length=255, nullable = true)
     */
    private $template;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable = true)
     */
    private $setFromDescription;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable = true)
     */
    private $setFromEmail;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime")
     */
    private $modified;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_send", type="integer")
     */
    private $countSend = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_email_to_send", type="integer")
     */
    private $countEmailToSend = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_being_send", type="boolean")
     */
    private $isBeingSend = false;

    /**
     * @ORM\ManyToMany(targetEntity="Improve\UserBundle\Entity\Group", cascade={"persist"})
     * @ORM\JoinTable(name="newsletter_accepted_group")
     */
    private $groupAccepted;
    
    /**
     * @ORM\ManyToMany(targetEntity="Improve\UserBundle\Entity\Group", cascade={"persist"})
     * @ORM\JoinTable(name="newsletter_declined_group")
     */
    private $groupDeclined;

    /**
     * @ORM\OneToMany(targetEntity="Improve\NewsletterBundle\Entity\NewsletterSender", mappedBy="newsletter")
     * @ORM\OrderBy({"created" = "DESC"})
     * @var Collection
     */
    protected $newsletterSenders;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setCreated(new \DateTime());
        $this->setModified(new \DateTime());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Newsletter
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Newsletter
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Newsletter
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Newsletter
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set countSend
     *
     * @param integer $countSend
     * @return Newsletter
     */
    public function setCountSend($countSend)
    {
        $this->countSend = $countSend;

        return $this;
    }

    /**
     * Get countSend
     *
     * @return integer
     */
    public function getCountSend()
    {
        return $this->countSend;
    }

    /**
     * Add categories
     *
     * @param \Application\Sonata\UserBundle\Entity\Group $categories
     * @return Newsletter
     */
    public function addCategory(\Application\Sonata\UserBundle\Entity\Group $categories)
    {
        $this->categories[] = $categories;

        return $this;
    }

    /**
     * Remove categories
     *
     * @param \Application\Sonata\UserBundle\Entity\Group $categories
     */
    public function removeCategory(\Application\Sonata\UserBundle\Entity\Group $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Add groupAccepted
     *
     * @param \Improve\UserBundle\Entity\Group $groupAccepted
     * @return Newsletter
     */
    public function addGroupAccepted(\Improve\UserBundle\Entity\Group $groupAccepted)
    {
        $this->groupAccepted[] = $groupAccepted;

        return $this;
    }

    /**
     * Remove groupAccepted
     *
     * @param \Improve\UserBundle\Entity\Group $groupAccepted
     */
    public function removeGroupAccepted(\Improve\UserBundle\Entity\Group $groupAccepted)
    {
        $this->groupAccepted->removeElement($groupAccepted);
    }

    /**
     * Get groupAccepted
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroupAccepted()
    {
        return $this->groupAccepted;
    }

    /**
     * Add groupDeclined
     *
     * @param \Improve\UserBundle\Entity\Group $groupDeclined
     * @return Newsletter
     */
    public function addGroupDeclined(\Improve\UserBundle\Entity\Group $groupDeclined)
    {
        $this->groupDeclined[] = $groupDeclined;

        return $this;
    }

    /**
     * Remove groupDeclined
     *
     * @param \Improve\UserBundle\Entity\Group $groupDeclined
     */
    public function removeGroupDeclined(\Improve\UserBundle\Entity\Group $groupDeclined)
    {
        $this->groupDeclined->removeElement($groupDeclined);
    }

    /**
     * Get groupDeclined
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroupDeclined()
    {
        return $this->groupDeclined;
    }

    /**
     */
    public function getLinkToSendNewsletter()
    {
        $url = "http://dev.improve/app_dev.php/newsletter/prepareUsersToSend/" . $this->getId();

        return $url;
    }

    /**
     * Set template
     *
     * @param string $template
     * @return Newsletter
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template
     *
     * @return string 
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Add newsletterSenders
     *
     * @param \Improve\NewsletterBundle\Entity\NewsletterSender $newsletterSenders
     * @return Newsletter
     */
    public function addNewsletterSender(\Improve\NewsletterBundle\Entity\NewsletterSender $newsletterSenders)
    {
        $this->newsletterSenders[] = $newsletterSenders;

        return $this;
    }

    /**
     * Remove newsletterSenders
     *
     * @param \Improve\NewsletterBundle\Entity\NewsletterSender $newsletterSenders
     */
    public function removeNewsletterSender(\Improve\NewsletterBundle\Entity\NewsletterSender $newsletterSenders)
    {
        $this->newsletterSenders->removeElement($newsletterSenders);
    }

    /**
     * Get newsletterSenders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNewsletterSenders()
    {
        return $this->newsletterSenders;
    }

    /**
     * Set countEmailToSend
     *
     * @param integer $countEmailToSend
     * @return Newsletter
     */
    public function setCountEmailToSend($countEmailToSend)
    {
        $this->countEmailToSend = $countEmailToSend;

        return $this;
    }

    /**
     * Get countEmailToSend
     *
     * @return integer 
     */
    public function getCountEmailToSend()
    {
        return $this->countEmailToSend;
    }

    /**
     * Set isBeingSend
     *
     * @param boolean $isBeingSend
     * @return Newsletter
     */
    public function setIsBeingSend($isBeingSend)
    {
        $this->isBeingSend = $isBeingSend;

        return $this;
    }

    /**
     * Get isBeingSend
     *
     * @return boolean 
     */
    public function getIsBeingSend()
    {
        return $this->isBeingSend;
    }

    /**
     * @return string
     */
    public function getSetFromDescription()
    {
        return $this->setFromDescription;
    }

    /**
     * @param string $setFromDescription
     */
    public function setSetFromDescription($setFromDescription)
    {
        $this->setFromDescription = $setFromDescription;
    }

    /**
     * @return string
     */
    public function getSetFromEmail()
    {
        return $this->setFromEmail;
    }

    /**
     * @param string $setFromEmail
     */
    public function setSetFromEmail($setFromEmail)
    {
        $this->setFromEmail = $setFromEmail;
    }
}
