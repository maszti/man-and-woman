<?php

namespace Improve\NewsletterBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NewsletterSender
 *
 * @ORM\Table("newsletter_sender")
 * @ORM\Entity(repositoryClass="Improve\NewsletterBundle\Entity\NewsletterSenderRepository")
 */
class NewsletterSender
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="send", type="boolean")
     */
    private $send = false;
    
    /**
     * @ORM\ManyToOne(targetEntity="Improve\NewsletterBundle\Entity\Newsletter", inversedBy="newsletterSenders", cascade={"persist"})
     */
    private $newsletter;

    /**
     * @ORM\ManyToOne(targetEntity="Improve\UserBundle\Entity\User", cascade={"persist"})
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set send
     *
     * @param boolean $send
     * @return NewsletterSender
     */
    public function setSend($send)
    {
        $this->send = $send;

        return $this;
    }

    /**
     * Get send
     *
     * @return boolean 
     */
    public function getSend()
    {
        return $this->send;
    }

    /**
     * Set newsletter
     *
     * @param \Improve\NewsletterBundle\Entity\Newsletter $newsletter
     * @return NewsletterSender
     */
    public function setNewsletter(\Improve\NewsletterBundle\Entity\Newsletter $newsletter = null)
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    /**
     * Get newsletter
     *
     * @return \Improve\NewsletterBundle\Entity\Newsletter 
     */
    public function getNewsletter()
    {
        return $this->newsletter;
    }

    /**
     * Set user
     *
     * @param \Improve\UserBundle\Entity\User $user
     * @return NewsletterSender
     */
    public function setUser(\Improve\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Improve\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
