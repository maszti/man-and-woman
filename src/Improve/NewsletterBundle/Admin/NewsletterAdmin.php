<?php

namespace Improve\NewsletterBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class NewsletterAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Ogólne', ['class' => 'col-md-8'])
                ->add('name', 'text', ['label' => 'Nazwa newslettera'])
                ->add('template', 'text', ['required' => false, 'label' => 'Szablon do wysyłki (name.html.twig)', 'data' => "default.html.twig"])
                ->add('content', 'ckeditor', ['config_name' => 'default', 'label' => 'Treść'])
                ->add('groupAccepted', null, ['required' => false, 'label' => 'Grupy docelowe do wysyłki'])
                ->add('groupDeclined', null, ['required' => false, 'label' => 'Grupy zablokowane do wysyłki'])
            ->end()
            ->with('Ustawienia', ['class' => 'col-md-4'])
                ->add('setFromEmail', 'text', ['required' => false, 'label' => 'Email z którego wysyłamy newsletter'])
                ->add('setFromDescription', 'text', ['required' => false, 'label' => 'Wyświetlany nadawca w meilach'])
            ->end();
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, ['label' => 'Nazwa']);
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name', null, ["label" => "Nazwa"])
            ->add('countEmailToSend', null, ["label" => "Liczba email do wysyłki"])
            ->add('isBeingSend', null, ["label" => "Wysłany"])
            ->add('_action', 'actions', [
                'actions' => [
                    'edit' => array(),
                    'delete' => [],
                ],
                'label' => 'Akcje'
            ]);
    }
}
?>