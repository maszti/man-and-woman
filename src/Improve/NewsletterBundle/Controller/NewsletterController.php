<?php

namespace Improve\NewsletterBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Improve\NewsletterBundle\Entity\Newsletter;
use Improve\NewsletterBundle\Entity\NewsletterSender;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/newsletter")
 */
class NewsletterController extends Controller
{
    /**
     * @Route("/sendNewsletter/{idNewsletter}", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function prepareUserToSendAction($idNewsletter = null)
    {
        if($idNewsletter == null) {
            throw new AccessDeniedException();
        }

        $newsletter = $this->getDoctrine()
            ->getRepository('ImproveNewsletterBundle:Newsletter')
            ->findOneById($idNewsletter);

        if($newsletter == null) {
            throw new AccessDeniedException();
        }

        $this->get("improve_newsletter.prepare_users")->prepareUsers($newsletter);

        return $this->redirect($this->generateUrl("admin_improve_newsletter_newsletter_list"));
    }
}
