<?php
namespace Improve\NewsletterBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Response;

class SendEmailCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('improve:newsletter:send')
            ->setDescription('Test SMTP newsletter');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->sendNewsletterToUser();
    }

    private function sendNewsletterToUser()
    {
        $mailer = $this->getContainer()->get('swiftmailer.mailer.test');

        $message = \Swift_Message::newInstance()
                    ->setSubject("Testowy newsletter")
                    ->setFrom(['dmasztalerz@e-improve.pl' => "Improve"])
                    ->setTo(['darek.masztalerz@gmail.com'])
                    ->setReplyTo(['dmasztalerz@e-improve.pl' => "Improve"])
                    ->setBody("TESTOWY");

        $mailer->send($message);
    }
}
