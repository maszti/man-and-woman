<?php

namespace Improve\NewsletterBundle\Service;
use Improve\NewsletterBundle\Entity\Newsletter;
use Improve\NewsletterBundle\Entity\NewsletterSender;

class PrepareUsersToSendNewsletterService
{
    private $em;
    private $container;

    public function __construct($em, $container)
    {
        $this->em = $em;
        $this->container = $container;
    }

    public function prepareUsers(Newsletter $newsletter)
    {
        list($acceptedGroupsIds, $declinedGroupsIds) = $this->getGroupsToSendNewsletter($newsletter);

        $users = $this->em->getRepository("ImproveUserBundle:User")->getUsersByGroupIds($acceptedGroupsIds, $declinedGroupsIds);
        if($users) {
            $this->prepareUsersToSend($users, $newsletter);
        }
    }

    private function getGroupsToSendNewsletter($newsletter)
    {
        $acceptedGroups = $newsletter->getGroupAccepted()->getValues();
        $declinedGroups = $newsletter->getGroupDeclined()->getValues();

        $acceptedGroupsIds = null;
        $declinedGroupsIds = null;

        foreach($acceptedGroups as $acceptedGroup) {
            $acceptedGroupsIds[] = $acceptedGroup->getId();
        }

        foreach($declinedGroups as $declinedGroup) {
            $declinedGroupsIds[] = $declinedGroup->getId();
        }

        return [$acceptedGroupsIds, $declinedGroupsIds];
    }

    private function prepareUsersToSend($users, $newsletter)
    {
        $mailer = $this->container->get('swiftmailer.mailer.newsletter')
            ->getTransport()
            ->getSpool();

        if(!$newsletter->getSetFromEmail()) {
            $fromEmail = "biuro@man-and-woman24.pl";
        } else {
            $fromEmail = $newsletter->getSetFromEmail();
        }

        if(!$newsletter->getSetFromDescription()) {
            $fromEmailDescription = "Redakcja Man&Woman";
        } else {
            $fromEmailDescription = $newsletter->getSetFromDescription();
        }

        foreach($users as $user) {
            $message = \Swift_Message::newInstance()
                ->setSubject($newsletter->getName())
                ->setFrom([$fromEmail => $fromEmailDescription])
                ->setTo($user->getEmail())
                ->setReplyTo([$fromEmail => $fromEmailDescription])
                ->setBody($newsletter->getContent(), 'text/html');
            $mailer->queueMessage($message);
        }

        $count = count($users);
        $newsletter->setCountEmailToSend($newsletter->getCountEmailToSend() + $count);
        if($newsletter->getCountEmailToSend() > 0) {
            $newsletter->setIsBeingSend(true);
        }
        $this->em->persist($newsletter);
        $this->em->flush();
    }
}
