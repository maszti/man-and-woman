<?php

namespace Improve\CoreBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class Advertisement extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Opis firmy', ['class' => 'col-md-8'])
                ->add('company', 'text', ["required" => false, 'label' => 'Nazwa firmy organizacji'])
                ->add('description', 'text', ["required" => false, 'label' => 'Opis firmy organizacji'])
            ->end()
            ->with('Dane adresowe', ['class' => 'col-md-4'])
                ->add('address', 'text', ["required" => false, 'label' => 'Adres'])
                ->add('city', 'text', ["required" => false, 'label' => 'Miasto'])
                ->add('phone', 'text', ["required" => false, 'label' => 'Telefon'])
                ->add('email', 'text', ["required" => false, 'label' => 'Email'])
            ->end();
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('company', null, ['label' => 'Nazwa firmy'])
            ->add('email', null, ['label' => 'Email']);
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('company', null, ["label" => "Id tagu"])
            ->add('email', null, ["label" => "Nazwa"])
            ->add('phone', null, ["label" => "Slug"])
            ->add('city', null, ["label" => "Aktywny"]);
    }
}
?>