<?php

namespace Improve\CoreBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class TagAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text', ['label' => 'Nazwa tagu'])
            ->add('active', 'text', ['label' => 'Aktywny tag'])
            ->add('type', 'text', ['required' => false, 'label' => 'Typ(kategoria lub tag)']);
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('type');
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, ["label" => "Id tagu"])
            ->addIdentifier('name', null, ["label" => "Nazwa"])
            ->add('slug', null, ["label" => "Slug"])
            ->add('active', null, ["label" => "Aktywny"])
            ->add('type', null, ["label" => "Typ"]);
    }
}
?>