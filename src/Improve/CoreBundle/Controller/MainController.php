<?php

namespace Improve\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class MainController extends Controller
{
    /**
     * @Route("/testowy", options={"expose"=true})
     * @Template()
     */
    public function indexAction(){

        return compact([]);
    }
}
