<?php

namespace Improve\CoreBundle\Service;
use Improve\UserBundle\Entity\User;

class ImportClientService
{
    private $em;

    public function __construct($em)
    {
         $this->em = $em;
    }

    public function addNewUserGd($phpExcelObject)
    {
        $activeSheets = $phpExcelObject->getActiveSheet();
        $count = $phpExcelObject->getActiveSheet()->getHighestRow();

        $groups1 = $this->em->getRepository('ImproveUserBundle:Group')
                      ->findByName("gmdt");
        $groups2 = $this->em->getRepository('ImproveUserBundle:Group')
                      ->findByName("private_user");

        for($i= 2; $i <= $count; $i++) {
            $username = $activeSheets->getCell("B" . $i)->getValue();
            $email = $activeSheets->getCell("D" . $i)->getValue();

            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $isEmail = $this->em->getRepository('ImproveUserBundle:User')
                      ->findByEmail($email);

                $isUsername = $this->em->getRepository('ImproveUserBundle:User')
                    ->findByUsername($username);

                if(!$isEmail && !$isUsername) {
                    $users = new User();

                    $users->setUsername($username);
                    $users->setEmail($email);
                    $users->setRoles(["ROLE_USER"]);
                    $users->setPassword($username);
                    $users->setEnabled(false);
                    $users->setGroups($groups1);
                    $users->setGroups($groups2);

                    $this->em->persist($users);
                }
            }
        }
    }

    public function addNewUsersPentagosHostess($phpExcelObject)
    {
        $activeSheets = $phpExcelObject->getActiveSheet();
        $count = $phpExcelObject->getActiveSheet()->getHighestRow();

        $groups1 = $this->em->getRepository('ImproveUserBundle:Group')
                      ->findByName("pentagos");
        $groups2 = $this->em->getRepository('ImproveUserBundle:Group')
                      ->findByName("private_user");

        for($i= 1; $i <= $count; $i++) {
            $firstname = $activeSheets->getCell("A" . $i)->getValue();
            $lastname = $activeSheets->getCell("B" . $i)->getValue();
            $dateBirthday = $activeSheets->getCell("C" . $i)->getValue();
            $address = $activeSheets->getCell("D" . $i)->getValue();
            $phone = $activeSheets->getCell("E" . $i)->getValue();
            $email = $activeSheets->getCell("F" . $i)->getValue();

            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $isEmail = $this->em->getRepository('ImproveUserBundle:User')
                      ->findByEmail($email);

                if(!$isEmail) {
                    $users = new User();
                    $users->setUsername($firstname . "_" . $lastname . "_" . $email);
                    $users->setFirstname($firstname);
                    $users->setLastname($lastname);
                    $users->setAddress($address);
                    $users->setPhone($phone);
                    $users->setDateBirthday($dateBirthday);
                    $users->setEmail($email);
                    $users->setRoles(["ROLE_USER"]);
                    $users->setPassword($firstname . " " . $lastname);
                    $users->setEnabled(false);
                    $users->setGroups($groups1);
                    $users->setGroups($groups2);

                    $this->em->persist($users);
                }
            }
        }
    }

    public function addNewUsersPentagosCompany($phpExcelObject)
    {
        $activeSheets = $phpExcelObject->getActiveSheet();
        $count = $phpExcelObject->getActiveSheet()->getHighestRow();

        $groups1 = $this->em->getRepository('ImproveUserBundle:Group')
                      ->findByName("pentagos");
        $groups2 = $this->em->getRepository('ImproveUserBundle:Group')
                      ->findByName("company");

        for($i= 1; $i <= $count; $i++) {
            $username = $activeSheets->getCell("A" . $i)->getValue();
            $firstname = $activeSheets->getCell("B" . $i)->getValue();
            $lastname = $activeSheets->getCell("C" . $i)->getValue();
            $email = $activeSheets->getCell("E" . $i)->getValue();

            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $isEmail = $this->em->getRepository('ImproveUserBundle:User')
                      ->findByEmail($email);

                if(!$isEmail) {
                    $users = new User();
                    $users->setUsername($username);
                    $users->setFirstname($firstname);
                    $users->setLastname($lastname);
                    $users->setEmail($email);
                    $users->setRoles(["ROLE_USER"]);
                    $users->setPassword($username);
                    $users->setEnabled(false);
                    $users->setGroups($groups1);
                    $users->setGroups($groups2);

                    $this->em->persist($users);
                }
            }
        }
    }

    public function addNewUsers40minut($phpExcelObject)
    {
        $activeSheets = $phpExcelObject->getActiveSheet();
        $count = $phpExcelObject->getActiveSheet()->getHighestRow();

        $groups1 = $this->em->getRepository('ImproveUserBundle:Group')
                      ->findByName("40minut");
        $groups2 = $this->em->getRepository('ImproveUserBundle:Group')
                      ->findByName("private_user");

        for($i= 1; $i <= $count; $i++) {
            $firstname = $activeSheets->getCell("A" . $i)->getValue();
            $lastname = $activeSheets->getCell("B" . $i)->getValue();
            $email = $activeSheets->getCell("C" . $i)->getValue();

            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $isEmail = $this->em->getRepository('ImproveUserBundle:User')
                      ->findByEmail($email);

                if(!$isEmail) {
                    $users = new User();
                    $users->setUsername($firstname . "_" . $lastname . "_" . $email);
                    $users->setFirstname($firstname);
                    $users->setLastname($lastname);
                    $users->setEmail($email);
                    $users->setRoles(["ROLE_USER"]);
                    $users->setPassword($firstname . "_" . $lastname);
                    $users->setEnabled(false);
                    $users->setGroups($groups1);
                    $users->setGroups($groups2);

                    $this->em->persist($users);
                }
            }
        }
    }

    public function addNewUsersNefretete($phpExcelObject)
    {
        $activeSheets = $phpExcelObject->getActiveSheet();
        $count = $phpExcelObject->getActiveSheet()->getHighestRow();

        $groups1 = $this->em->getRepository('ImproveUserBundle:Group')
                      ->findByName("nefretete");
        $groups2 = $this->em->getRepository('ImproveUserBundle:Group')
                      ->findByName("private_user");

        for($i= 1; $i <= $count; $i++) {
            $firstname = $activeSheets->getCell("A" . $i)->getValue();
            $lastname = $activeSheets->getCell("B" . $i)->getValue();
            $email = $activeSheets->getCell("D" . $i)->getValue();
            $phone = $activeSheets->getCell("E" . $i)->getValue();

            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $isEmail = $this->em->getRepository('ImproveUserBundle:User')
                      ->findByEmail($email);

                if(!$isEmail) {
                    $users = new User();
                    $users->setUsername($firstname . "_" . $lastname . "_" . $email);
                    $users->setFirstname($firstname);
                    $users->setLastname($lastname);
                    $users->setEmail($email);
                    $users->setPhone($phone);
                    $users->setRoles(["ROLE_USER"]);
                    $users->setPassword($firstname . "_" . $lastname);
                    $users->setEnabled(false);
                    $users->setGroups($groups1);
                    $users->setGroups($groups2);

                    $this->em->persist($users);
                }
            }
        }
    }
    
    public function addNewUsersTest($phpExcelObject)
    {
        $activeSheets = $phpExcelObject->getActiveSheet();
        $count = $phpExcelObject->getActiveSheet()->getHighestRow();

        $groups1 = $this->em->getRepository('ImproveUserBundle:Group')
                      ->findByName("testowy");
        $groups2 = $this->em->getRepository('ImproveUserBundle:Group')
                      ->findByName("private_user");

        for($i= 1; $i <= $count; $i++) {
            $username = $activeSheets->getCell("A" . $i)->getValue();
            $email = $activeSheets->getCell("C" . $i)->getValue();
            ld($email);
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $isEmail = $this->em->getRepository('ImproveUserBundle:User')
                      ->findByEmail($email);

                if(!$isEmail) {
                    $users = new User();
                    $users->setUsername($username);
                    $users->setEmail($email);
                    $users->setRoles(["ROLE_USER"]);
                    $users->setPassword($username);
                    $users->setEnabled(false);
                    $users->setGroups($groups1);
                    $users->setGroups($groups2);
                    $this->em->persist($users);
                }
            }
        }
    }
}
