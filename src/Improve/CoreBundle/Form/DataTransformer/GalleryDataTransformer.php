<?php

namespace Improve\CoreBundle\Form\DataTransformer;

use Application\Sonata\MediaBundle\Entity\GalleryHasMedia;
use Application\Sonata\MediaBundle\Entity\Media;
use Symfony\Component\Form\DataTransformerInterface as BaseTransformer;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Collections\ArrayCollection;

class GalleryDataTransformer implements BaseTransformer
{
    /**
     */
    public function transform($value) {}

    /**
     * Transforms a array files to an collections media.
     *
     * @param  Array $files
     *
     * {@inheritdoc}
     */
    public function reverseTransform($data)
    {
        $files = $data;
        $galleryHasMediaCollections = new ArrayCollection();

        foreach($files as $key => $file) {
            $galleryHasMedia = new GalleryHasMedia();

            $media = new Media;
            $media->setBinaryContent($file);
            $media->setContext('gallery_upload');
            $media->setProviderName('sonata.media.provider.image');

            $galleryHasMedia->setMedia($media);
            $galleryHasMedia->setPosition($key);
            $galleryHasMedia->setEnabled(true);

            $galleryHasMediaCollections->add($galleryHasMedia);
        }

        return $galleryHasMediaCollections;
    }
}
