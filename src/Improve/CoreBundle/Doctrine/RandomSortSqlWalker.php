<?php

namespace Improve\CoreBundle\Doctrine;

use Doctrine\ORM\Query;

/**
 * The SortableNullsWalker is a TreeWalker that walks over a DQL AST and constructs
 * the corresponding SQL to allow ORDER BY x ASC NULLS FIRST|LAST.
 *
 * [use]
 * $qb = $em->createQueryBuilder()
 *            ->select('p')
 *            ->from('Improve\ShopBundle\Entity\Product', 'p')
 *            ->addOrderBy('p.name', 'DESC')
 *            ->addOrderBy('p.gardensId', 'DESC'); // relation to person
 *
 * $query = $qb->getQuery();
 * $query->setHint(Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Improve\CoreBundle\Doctrine\RandomSortSqlWalker');
 */
class RandomSortSqlWalker extends Query\SqlWalker
{
    public function walkOrderByItem($orderByItem)
    {
        return "RANDOM()";
    }
}
