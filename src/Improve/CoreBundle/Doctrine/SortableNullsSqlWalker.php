<?php

namespace Improve\CoreBundle\Doctrine;

use Doctrine\ORM\Query;

/**
 * The SortableNullsWalker is a TreeWalker that walks over a DQL AST and constructs
 * the corresponding SQL to allow ORDER BY x ASC NULLS FIRST|LAST.
 *
 * [use]
 * $qb = $em->createQueryBuilder()
 *            ->select('p')
 *            ->from('Improve\ShopBundle\Entity\Product', 'p')
 *            ->addOrderBy('p.name', 'DESC')
 *
 * $query = $qb->getQuery();
 * $query->setHint(Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER, 'Improve\CoreBundle\Doctrine\SortableNullsSqlWalker');
 * $query->setHint("sortableNulls.fields", array(
 *                "p.name" => Improve\CoreBundle\Doctrine\SortableNullsSqlWalker::NULLS_FIRST
 *            ));
 */
class SortableNullsSqlWalker extends Query\SqlWalker
{
    const NULLS_FIRST = 'NULLS FIRST';
    const NULLS_LAST = 'NULLS LAST';

    public function walkOrderByItem($orderByItem)
    {
        $sql = parent::walkOrderByItem($orderByItem);
        $hint = $this->getQuery()->getHint('sortableNulls.fields');
        $expr = $orderByItem->expression;
        $type = strtoupper($orderByItem->type);

        if (is_array($hint) && count($hint)) {
            if (
                $expr instanceof Query\AST\PathExpression &&
                $expr->type == Query\AST\PathExpression::TYPE_STATE_FIELD
            ) {
                $dqlAlias = $expr->identificationVariable . (!empty($expr->field) ? '.' . $expr->field : '');
                $search = $this->walkPathExpression($expr) . ' ' . $type;
                $sql = $this->updateOrderBy($dqlAlias, $hint, $search, $sql);
            } elseif(is_string($expr)) {
                $search = $this->walkResultVariable($this->getQueryComponent($expr)['token']['value']);
                $search .= " " . $type;
                $sql = $this->updateOrderBy($expr, $hint, $search, $sql);
            }
        }

        return $sql;
    }

    /**
     * @param $dqlAlias
     * @param $hint
     * @param $search
     * @param $sql
     * @return mixed
     */
    private function updateOrderBy($dqlAlias, $hint, $search, $sql)
    {
        if (array_key_exists($dqlAlias, $hint)) {
            $sql = str_replace($search, $search . ' ' . $hint[$dqlAlias], $sql);
        }

        return $sql;
    }
}
