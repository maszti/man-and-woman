<?php

namespace Maw\MessageBundle\Block;

use Doctrine\ORM\EntityManager;
use Maw\MessageBundle\Entity\Board;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\BlockBundle\Block\BaseBlockService;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CommentListBlock extends BaseBlockService
{
    const TEMPLATE_COMMENT_LIST = 'MawMessageBundle:Block:commentList.html.twig';

    public function __construct($name, EngineInterface $templating, EntityManager $em)
    {
        $this->name = $name;
        $this->templating = $templating;
        $this->em = $em;
    }

    public function setDefaultSettings(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'template' => self::TEMPLATE_COMMENT_LIST,
            'boardId' => null
        ]);
    }

    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $settings = $blockContext->getSettings();

        $items = $this->getCommentList($settings["boardId"]);
        return $this->renderResponse(self::TEMPLATE_COMMENT_LIST, ['items' => $items], $response)->setTtl(0);
    }

    private function getCommentList($boardId)
    {
        /** @var Board $board */
        $board = $this->em
            ->getRepository('MawMessageBundle:Board')
            ->findOneBy(['id' => $boardId]);

        return $board->getPosts();

    }
}