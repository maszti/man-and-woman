<?php

namespace Maw\MessageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Improve\UserBundle\Entity\User;

/**
 * Post
 *
 * @ORM\Table("maw_message_post")
 * @ORM\Entity
 */
class Post
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="string", length=255, nullable=true)
     */
    private $author;
    
    /**
     * @ORM\ManyToOne(targetEntity="Improve\UserBundle\Entity\User")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", length=500)
     */
    private $content;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_like", type="integer")
     */
    private $countLike = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_unlike", type="integer")
     */
    private $countUnlike = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @ORM\ManyToOne(targetEntity="Maw\MessageBundle\Entity\Board", inversedBy="posts")
     */
    private $board;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setCreated(new \DateTime("now"));
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set author
     *
     * @param string $author
     * @return Post
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Post
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set countLike
     *
     * @param integer $countLike
     * @return Post
     */
    public function setCountLike($countLike)
    {
        $this->countLike = $countLike;

        return $this;
    }

    /**
     * Get countLike
     *
     * @return integer 
     */
    public function getCountLike()
    {
        return $this->countLike;
    }

    /**
     * Set countUnlike
     *
     * @param integer $countUnlike
     * @return Post
     */
    public function setCountUnlike($countUnlike)
    {
        $this->countUnlike = $countUnlike;

        return $this;
    }

    /**
     * Get countUnlike
     *
     * @return integer 
     */
    public function getCountUnlike()
    {
        return $this->countUnlike;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Post
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set board
     *
     * @param \Maw\MessageBundle\Entity\Board $board
     * @return Post
     */
    public function setBoard(\Maw\MessageBundle\Entity\Board $board = null)
    {
        $this->board = $board;

        return $this;
    }

    /**
     * Get board
     *
     * @return \Maw\MessageBundle\Entity\Board 
     */
    public function getBoard()
    {
        return $this->board;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return Post
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}
