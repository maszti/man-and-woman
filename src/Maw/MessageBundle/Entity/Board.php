<?php

namespace Maw\MessageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Board
 *
 * @ORM\Table("maw_message_board")
 * @ORM\Entity
 */
class Board
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_post", type="integer")
     */
    private $countPost = 0;

    /**
     * @ORM\OneToMany(targetEntity="Maw\MessageBundle\Entity\Post", mappedBy="board")
     */
    private $posts;

    /**
     * @ORM\OneToOne(targetEntity="Maw\NewsBundle\Entity\News", mappedBy="board")
     */
    private $news;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * Constructor
     */
    public function __construct($news = null)
    {
        $this->posts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setNews($news);
        $this->setCreated(new \DateTime('now'));
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set countPost
     *
     * @param integer $countPost
     * @return Board
     */
    public function setCountPost($countPost)
    {
        $this->countPost = $countPost;

        return $this;
    }

    /**
     * Get countPost
     *
     * @return integer 
     */
    public function getCountPost()
    {
        return $this->countPost;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Board
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Add posts
     *
     * @param \Maw\MessageBundle\Entity\Post $posts
     * @return Board
     */
    public function addPost(\Maw\MessageBundle\Entity\Post $posts)
    {
        $this->posts[] = $posts;

        return $this;
    }

    /**
     * Remove posts
     *
     * @param \Maw\MessageBundle\Entity\Post $posts
     */
    public function removePost(\Maw\MessageBundle\Entity\Post $posts)
    {
        $this->posts->removeElement($posts);
    }

    /**
     * Get posts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * Set news
     *
     * @param \Maw\NewsBundle\Entity\News $news
     * @return Board
     */
    public function setNews(\Maw\NewsBundle\Entity\News $news = null)
    {
        $this->news = $news;

        return $this;
    }

    /**
     * Get news
     *
     * @return \Maw\NewsBundle\Entity\News 
     */
    public function getNews()
    {
        return $this->news;
    }
}
