<?php

namespace Maw\MessageBundle\Controller;

use Maw\MessageBundle\Entity\Board;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Maw\MessageBundle\Form\PostType;
use Maw\MessageBundle\Entity\Post;

class BoardController extends Controller
{
    /**
     * @Route("/board/{id}", options={"expose"=true})
     * @Template()
     */
    public function addPostAction(Request $request, $id)
    {
        /** @var Board $board */
        $board = $this->getDoctrine()
            ->getRepository('MawMessageBundle:Board')
            ->findOneBy(['id' => $id]);

        if(!$board) {
            $this->addFlash('error', "Błąd przy Tworzeniu kometarza.");
            throw $this->createNotFoundException('Taka tablica nie istnieje.');
        }

        /** @var Post $post */
        $post = new Post();
        $formType = $this->createForm(new PostType(), $post);
        if ($request->getMethod() == 'POST') {
            if ($formType->submit($request) && $formType->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $post = $formType->getData();

                if($this->getUser()) {
                    $post->setUser($this->getUser());
                }

                $post->setBoard($board);
                $em->persist($post);
                $em->flush();

                $this->addFlash('success', "Twój komentarz został dodany.");
            } else {
                $validator = $this->get('validator');
                $errors = $validator->validate($post);
                $this->addFlash('error', "Nie dodano Twojego komentarza. Wystąpiły następujące błędy: " . $errors[0]->getMessage());
            }
        } else {
            $this->addFlash('error', "Nie dodano Twojego komentarza.");
        }

        return $this->redirect($this->generateUrl("maw_news_news_newsview", ["slug" => $board->getNews()->getSlug()]));
    }
}
