<?php

namespace Maw\MessageBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', 'textarea', ['label' => false, 'max_length' => 500])
            ->add('author', null, ['label' => false, 'data' => 'gość'])
            ->add('user', null, ['label' => false]);
            /*->add('captcha', "genemu_captcha", array('label' => 'Wpisz kod z obrazka:', "mapped" => false));*/
    }

    public function getName()
    {
        return 'maw_messagebundle_posttype';
    }
}
