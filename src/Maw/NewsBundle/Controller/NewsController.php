<?php
namespace Maw\NewsBundle\Controller;

use Maw\MessageBundle\Entity\Post;
use Maw\MessageBundle\Form\PostType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class NewsController extends Controller
{
    /**
     * @Route("/", options={"expose"=true})
     * @Template()
     */
    public function indexAction()
    {
        $manArticles = $this->getDoctrine()
            ->getRepository('MawNewsBundle:News')
            ->getArticlesByCategory('facet', 5);

        $womanArticles = $this->getDoctrine()
            ->getRepository('MawNewsBundle:News')
            ->getArticlesByCategory('kobieta', 5);

        return compact("manArticles", "womanArticles");
    }

    /**
     * @Route("/newsy/{slugCategory}/{slugTag}", options={"expose"=true})
     * @Template()
     */
    public function newsListAction($slugCategory, $slugTag)
    {
        $category = $this->getDoctrine()
            ->getRepository('ImproveCoreBundle:Tag')
            ->getTagBySlug($slugCategory);

        if(!$category) {
            throw $this->createNotFoundException('Taka kategoria nie istnieje.');
        }

        $tag = $this->getDoctrine()
            ->getRepository('ImproveCoreBundle:Tag')
            ->getTagBySlug($slugTag);

        if(!$tag) {
            throw $this->createNotFoundException('Taki dział nie istnieje.');
        }

        $articlesQb = $this->getDoctrine()
            ->getRepository('MawNewsBundle:News')
            ->getListArticlesByCategoryAndTagQb($category, $tag);

        $paginator = $this->get('knp_paginator');
        $articles = $paginator->paginate($articlesQb, $this->get('request')->query->get('page', 1), 5);
        $countArticles = $articles->count();

        return compact("articles", "countArticles");
    }

    /**
     * @Route("/news,{slug}", options={"expose"=true})
     * @Template()
     */
    public function newsViewAction($slug)
    {
        $article = $this->getDoctrine()
            ->getRepository('MawNewsBundle:News')
            ->findOneBySlug($slug);

        if (!$article) {
            throw $this->createNotFoundException('Taki artykuł nie istnieje.');
        }

        $post = new Post();
        $formType = $this->createForm(new PostType(), $post);
        $form = $formType->createView();

        return compact("article", "form");
    }
}
/*
 * color niebieski: 088cc8, kolor rozowy: ec3995
 */