<?php

namespace Maw\NewsBundle\Block;

use Doctrine\ORM\EntityManager;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Sonata\BlockBundle\Model\BlockInterface;

use Sonata\BlockBundle\Block\BaseBlockService;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NewsListBlock extends BaseBlockService
{
    const CACHE_TIME = 3600;
    const TEMPLATE_LEFT_BLOCK = 'MawNewsBundle:Block:threeColumnLeftNewsList.html.twig';
    const TEMPLATE_RIGHT_BLOCK = 'MawNewsBundle:Block:threeColumnRightNewsList.html.twig';

    public function __construct($name, EngineInterface $templating, EntityManager $em)
    {
        $this->name = $name;
        $this->templating = $templating;
        $this->em = $em;
    }

    public function setDefaultSettings(OptionsResolverInterface $resolver)
    {

        $resolver->setDefaults([
            'template' => self::TEMPLATE_LEFT_BLOCK,
            'category' => null,
            'limit' => 3
        ]);
    }

    public function getCacheKeys(BlockInterface $block)
    {
        return [
            'type' => $this->name
        ];
    }

    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $settings = $blockContext->getSettings();

        if($settings["category"] == "facet") {
            $template = self::TEMPLATE_LEFT_BLOCK;
            $items = $this->getNewsList($settings["category"], $settings["limit"]);
        } else {
            $template = self::TEMPLATE_RIGHT_BLOCK;
            $items = $this->getNewsList($settings["category"], $settings["limit"]);
        }

        return $this->renderResponse($template, ['items' => $items], $response)->setTtl(self::CACHE_TIME);
    }

    private function getNewsList($category, $limit)
    {
        return $this->em
            ->getRepository("MawNewsBundle:News")
            ->getRandArticlesByCategory($category, $limit);
    }
}