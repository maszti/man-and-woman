<?php
namespace Maw\NewsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class Source extends Admin
{
    public $supportsPreviewMode = true;
    private $securityContext;

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_by' => 'created',
        '_sort_order' => 'DESC'
    );

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Ogólne', ['class' => 'col-md-8'])
                ->add('title', null, ['label' => "Nazwa"])
                ->add('link', null, ['label' => "Link do źródła"])
            ->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title', null, ['label' => "Nazwa"]);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title', null, ['label' => "Imie"])
            ->add("link", null, ['label' => "Nazwisko"]);
    }

    public function setSecurityContext($securityContext)
    {
        $this->securityContext = $securityContext;
    }

    protected function getSecurityContext()
    {
        return $this->securityContext;
    }
}

