<?php
namespace Maw\NewsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class News extends Admin
{
    public $supportsPreviewMode = true;
    private $securityContext;

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_by' => 'publishDate',
        '_sort_order' => 'DESC'
    );

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Ogólne', ['class' => 'col-md-8'])
                ->add('title', null, ['label' => "Tytuł"])
                ->add('excerpt', 'ckeditor', ['config_name' => 'default', 'label' => "Zajawka"])
                ->add('content', 'ckeditor', ['config_name' => 'default', 'label' => "Treść"])
            ->end()
            ->with('Ustawienia', ['class' => 'col-md-4'])
                ->add('publishDate', 'sonata_type_datetime_picker', ['label' => "Data opublikowania", 'dp_side_by_side' => true, 'format'=> "dd.MM.yyyy, HH:mm:ss"])
                ->add('excerptMedia', 'sonata_type_model_list', ["required" => false, 'label' => "Obrazek na stronie głównej", 'help' => "Obrazek na stronie głównej (najlepsze wymiary: 481x 320px). Nie można wgrywać plików .png", 'btn_delete' => false], ['link_parameters' => ['context' => 'news']])
                ->add('contentMedia', 'sonata_type_model_list', ["required" => false, 'label' => "Obrazek do treci", 'btn_delete' => false, 'help' => "Obrazek na stronie newsa (najlepsze wymiary: 465px szerokości)"], ['link_parameters' => ['context' => 'news']])
                ->add('published', null, ["required" => false, 'label' => "Opublikowane"])
            ->end()
            ->with('Klasyfikacja artykułu', ['class' => 'col-md-4'])
                ->add('tags', null, ["query_builder" => $this->getTagsQb(), 'label' => "Tagi newsa"])
                ->add('category', null, ["query_builder" => $this->getCategoryQb(), 'label' => "Kategoria newsa"])
            ->end()
            ->with('Galeria', ['class' => 'col-md-4', 'description' => 'Dopisz galerie zdjęć do newsa. Przy wyborze galerii zmieniamy kontekst na gallery_upload !'])
                ->add('gallery', 'sonata_type_model_list', ['label' => "Wybierz galerie", 'required'  => false, 'btn_add' => false])
            ->end()
            ->with('Ustaw tylko jeśli potrzebujesz', ['class' => 'col-md-4'])
                ->add('slug', null, ["required" => false, 'label' => "Nazwa w URL", "help" => "np. Poprawny URL: 'abcd', Niepoprawny URL: '/abcd' "])
            ->end();
        if ($this->isGranted('MASTER')) {
            $formMapper
                ->with('Ustaw tylko jeśli potrzebujesz', ['class' => 'col-md-4'])
                    ->add('author', 'sonata_type_model_list', ['btn_add' => false, 'btn_delete' => false, 'label' => "Autor"])
                ->end();
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('slug', null, ['label' => "Nazwa w URL"])
            ->add('created', null, ['label' => "Utworzono"])
            ->add('published', null, ['label' => "Opublikowany"]);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title', null, ['label' => "Tytuł"])
            ->addIdentifier("slug", null, ['label' => "Nazwa w URL"])
            ->add("created", null, ['label' => "Utworzono"])
            ->add('publishDate', null, ['label' => "Data publikacji"])
            ->add('tags', null, ['label' => "Tagi newsa"])
            ->add('category', null, ['label' => "Kategoria"])
            ->add('published', null, ['label' => "Opublikowano", 'editable' => $this->isGranted('MASTER')]);
    }

    public function getNewInstance()
    {
        $entity = parent::getNewInstance();

        $user = $this->getSecurityContext()->getToken()->getUser();
        $entity->setAuthor($user);

        return $entity;
    }

    protected function getTagsQb()
    {
        return $this->modelManager->getEntityManager('ImproveCoreBundle:Tag')
            ->getRepository('ImproveCoreBundle:Tag')
            ->getTagsByContextsQb("tag");
    }

    protected function getCategoryQb()
    {
        return $this->modelManager->getEntityManager('ImproveCoreBundle:Tag')
            ->getRepository('ImproveCoreBundle:Tag')
            ->getTagsByContextsQb("category");
    }

    public function setSecurityContext($securityContext)
    {
        $this->securityContext = $securityContext;
    }

    protected function getSecurityContext()
    {
        return $this->securityContext;
    }
}

