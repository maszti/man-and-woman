<?php
namespace Maw\NewsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class Star extends Admin
{
    public $supportsPreviewMode = true;
    private $securityContext;

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_by' => 'created',
        '_sort_order' => 'DESC'
    );

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Ogólne', ['class' => 'col-md-8'])
                ->add('linkToFacebook', null, ['label' => "Link do facebooka"])
                ->add('linkToTwitter', null, ['label' => "Link do twittera"])
                ->add('linkToInstagram', null, ['label' => "Link do instagrama"])
                ->add('linkToOwnPage', null, ['label' => "Link do strony gwiazdy"])
            ->end()
            ->with('Dane gwiazdy', ['class' => 'col-md-4'])
                ->add('alias', null, ["required" => false, 'label' => "Pseudonim"])
                ->add('firstname', null, ["required" => false, 'label' => "Imie"])
                ->add('lastname', null, ["required" => false, 'label' => "Nazwisko"])
            ->end();
        if ($this->isGranted('MASTER')) {
            $formMapper
                ->with('Dane kontaktowe', ['class' => 'col-md-4'])
                    ->add('email', null, ["required" => false, 'label' => "Email"])
                    ->add('phone', null, ["required" => false, 'label' => "Telefon"])
                ->end();
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('alias', null, ['label' => "Pseudonim"])
            ->add('firstname', null, ['label' => "Nazwa w URL"])
            ->add('lastname', null, ['label' => "Utworzono"]);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('firstname', null, ['label' => "Imie"])
            ->addIdentifier("lastname", null, ['label' => "Nazwisko"])
            ->addIdentifier("alias", null, ['label' => "Pseudonim"])
            ->add('linkToInstagram', null, ['label' => "Link do instagrama"])
            ->add('linkToFacebook', null, ['label' => "Link do facebooka"]);
    }

    public function setSecurityContext($securityContext)
    {
        $this->securityContext = $securityContext;
    }

    protected function getSecurityContext()
    {
        return $this->securityContext;
    }
}

