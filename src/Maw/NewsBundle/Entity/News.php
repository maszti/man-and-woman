<?php

namespace Maw\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Improve\CoreBundle\Entity\Tag;
use Maw\MessageBundle\Entity\Board;

/**
 * News
 *
 * @ORM\Table("maw_news_news")
 * @ORM\Entity(repositoryClass="Maw\NewsBundle\Entity\NewsRepository")
 */
class News
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="excerpt", type="text")
     */
    private $excerpt;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var boolean
     *
     * @ORM\Column(name="published", type="boolean")
     */
    private $published = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publishDate", type="datetime")
     */
    private $publishDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime")
     */
    private $modified;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media")
     */
    private $excerptMedia;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media")
     */
    private $contentMedia;

    /**
     * @ORM\ManyToOne(targetEntity="Improve\UserBundle\Entity\User")
     */
    private $author;

    /**
     * @ORM\ManyToOne(targetEntity="Improve\CoreBundle\Entity\Tag")
     */
    private $category;

    /**
     * @ORM\OneToOne(targetEntity="Maw\MessageBundle\Entity\Board", inversedBy="news", cascade={"persist"})
     */
    private $board;

    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity="Improve\CoreBundle\Entity\Tag")
     * @ORM\JoinTable(name="maw_news_tags",
     *      joinColumns={@ORM\JoinColumn(name="news_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")}
     *      )
     **/
    private $tags;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Gallery")
     */
    private $gallery;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setCreated(new \DateTime("now"));
        $this->setModified(new \DateTime("now"));
        $this->setBoard(new Board($this));
    }

    public function getRoute()
    {
        return 'maw_news_news_newsview';
    }

    public function getRouteParameters()
    {
        return [
            'slug' => $this->getSlug()
        ];
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return News
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return News
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set excerpt
     *
     * @param string $excerpt
     * @return News
     */
    public function setExcerpt($excerpt)
    {
        $this->excerpt = $excerpt;

        return $this;
    }

    /**
     * Get excerpt
     *
     * @return string 
     */
    public function getExcerpt()
    {
        return $this->excerpt;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return News
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set published
     *
     * @param boolean $published
     * @return News
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return boolean 
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set publishDate
     *
     * @param \DateTime $publishDate
     * @return News
     */
    public function setPublishDate($publishDate)
    {
        $this->publishDate = $publishDate;

        return $this;
    }

    /**
     * Get publishDate
     *
     * @return \DateTime 
     */
    public function getPublishDate()
    {
        return $this->publishDate;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return News
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return News
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set excerptMedia
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $excerptMedia
     * @return News
     */
    public function setExcerptMedia(\Application\Sonata\MediaBundle\Entity\Media $excerptMedia = null)
    {
        $this->excerptMedia = $excerptMedia;

        return $this;
    }

    /**
     * Get excerptMedia
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media 
     */
    public function getExcerptMedia()
    {
        return $this->excerptMedia;
    }

    /**
     * Set contentMedia
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $contentMedia
     * @return News
     */
    public function setContentMedia(\Application\Sonata\MediaBundle\Entity\Media $contentMedia = null)
    {
        $this->contentMedia = $contentMedia;

        return $this;
    }

    /**
     * Get contentMedia
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media 
     */
    public function getContentMedia()
    {
        return $this->contentMedia;
    }

    /**
     * Set author
     *
     * @param \Improve\UserBundle\Entity\User $author
     * @return News
     */
    public function setAuthor(\Improve\UserBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \Improve\UserBundle\Entity\User 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set category
     *
     * @param Tag $category
     * @return News
     */
    public function setCategory(Tag $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return Tag
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add tags
     *
     * @param Tag $tags
     * @return News
     */
    public function addTag(Tag $tags)
    {
        $this->tags[] = $tags;

        return $this;
    }

    /**
     * Remove tags
     *
     * @param Tag $tags
     */
    public function removeTag(Tag $tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set board
     *
     * @param \Maw\MessageBundle\Entity\Board $board
     * @return News
     */
    public function setBoard(\Maw\MessageBundle\Entity\Board $board = null)
    {
        $this->board = $board;

        return $this;
    }

    /**
     * Get board
     *
     * @return \Maw\MessageBundle\Entity\Board 
     */
    public function getBoard()
    {
        return $this->board;
    }

    /**
     * Set gallery
     *
     * @param \Application\Sonata\MediaBundle\Entity\Gallery $gallery
     * @return News
     */
    public function setGallery(\Application\Sonata\MediaBundle\Entity\Gallery $gallery = null)
    {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * Get gallery
     *
     * @return \Application\Sonata\MediaBundle\Entity\Gallery 
     */
    public function getGallery()
    {
        return $this->gallery;
    }
}
