<?php

namespace Maw\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Star
 *
 * @ORM\Table("maw_news_star")
 * @ORM\Entity()
 */
class Star
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255, nullable=true)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="alias", type="string", length=255, nullable=true)
     */
    private $alias;

    /**
     * @var string
     *
     * @ORM\Column(name="linkToFacebook", type="string", length=255, nullable=true)
     */
    private $linkToFacebook;

    /**
     * @var string
     *
     * @ORM\Column(name="linkToTwitter", type="string", length=255, nullable=true)
     */
    private $linkToTwitter;

    /**
     * @var string
     *
     * @ORM\Column(name="linkToInstagram", type="string", length=255, nullable=true)
     */
    private $linkToInstagram;

    /**
     * @var string
     *
     * @ORM\Column(name="linkToOwnPage", type="string", length=255, nullable=true)
     */
    private $linkToOwnPage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthdate", type="datetime", nullable=true)
     */
    private $birthdate;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setCreated(new \DateTime("now"));
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    }

    /**
     * @return string
     */
    public function getLinkToFacebook()
    {
        return $this->linkToFacebook;
    }

    /**
     * @param string $linkToFacebook
     */
    public function setLinkToFacebook($linkToFacebook)
    {
        $this->linkToFacebook = $linkToFacebook;
    }

    /**
     * @return string
     */
    public function getLinkToTwitter()
    {
        return $this->linkToTwitter;
    }

    /**
     * @param string $linkToTwitter
     */
    public function setLinkToTwitter($linkToTwitter)
    {
        $this->linkToTwitter = $linkToTwitter;
    }

    /**
     * @return string
     */
    public function getLinkToInstagram()
    {
        return $this->linkToInstagram;
    }

    /**
     * @param string $linkToInstagram
     */
    public function setLinkToInstagram($linkToInstagram)
    {
        $this->linkToInstagram = $linkToInstagram;
    }

    /**
     * @return string
     */
    public function getLinkToOwnPage()
    {
        return $this->linkToOwnPage;
    }

    /**
     * @param string $linkToOwnPage
     */
    public function setLinkToOwnPage($linkToOwnPage)
    {
        $this->linkToOwnPage = $linkToOwnPage;
    }

    /**
     * @return \DateTime
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * @param \DateTime $birthdate
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }
}