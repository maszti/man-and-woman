<?php

namespace Maw\PortalBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

class MainController extends Controller
{
    /**
     * @Route("/", options={"expose"=true})
     * @Template()
     */
    public function indexAction()
    {
        return compact([]);
    }

    /**
     * @Route("/regulamin", options={"expose"=true})
     * @Template()
     */
    public function regulationsAction()
    {
        return compact([]);
    }

    /**
     * @Route("/polityka-prywatnosci", options={"expose"=true})
     * @Template()
     */
    public function policyAction()
    {
        return compact([]);
    }

    /**
     * @Route("/kariera", options={"expose"=true})
     * @Template()
     */
    public function cooperateAction()
    {
        return compact([]);
    }

    /**
     * @Route("/kontakt", options={"expose"=true})
     * @Template()
     */
    public function contactAction()
    {
        return compact([]);
    }
}
