$(document).ready(function() {

    $("#fos_user_registration_form_username" ).change(function() {
        var path = $("#register_form").attr("data-path");
        var username = $("#fos_user_registration_form_username").val();

        $.ajax({
            type: 'GET',
            url: path + '?username=' + username,
            dataType: "json",
            contentType: 'application/json',
            success: function(data) {
                $("#register_username .error_form ul").remove();
                if(data == 1) {
                    $("#register_username .error_form").append("<ul><li>Istnieje użytkownik o takiej nazwie.</li></ul>");
                }
            }
        });
    });

    $("#fos_user_registration_form_email" ).change(function() {
        var path = $("#register_form").attr("data-path");
        var email = $("#fos_user_registration_form_email").val();

        $.ajax({
            type: 'GET',
            url: path + '?email=' + email,
            dataType: "json",
            contentType: 'application/json',
            success: function(data) {
                $("#register_email .error_form ul").remove();
                if(data == 1) {
                    $("#register_email .error_form").append("<ul><li>Wpisany email jest już zajęty.</li></ul>");
                }
            }
        });
    });

    $("#fos_user_registration_form_agreeRegulation" ).change(function() {
        var regulations = $("#fos_user_registration_form_agreeRegulation").is(":checked");
        if(regulations == false) {
            $("#regulation .error_form ul").remove();
            $("#regulation .error_form").append("<ul><li>Akcetacja regulaminu jest wymagana. </li></ul>");
            return false;
        } else {
            $("#regulation .error_form ul").remove();
        }
    });

    $("form#register_form").submit(function(event) {
        var regulations = $("#fos_user_registration_form_agreeRegulation").is(":checked");
        if(regulations == false) {
            $("#regulation .error_form ul").remove();
            $("#regulation .error_form").append("<ul><li>Akcetacja regulaminu jest wymagana. </li></ul>");
            return false;
        }

        if($("#register_username .error_form ul li").text().length > 0) {
            return false;
        }

        if($("#register_email .error_form ul li").text().length > 0) {
            return false;
        }

        return true;
    });
});